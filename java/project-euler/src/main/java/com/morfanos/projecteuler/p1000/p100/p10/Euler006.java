package com.morfanos.projecteuler.p1000.p100.p10;

import com.morfanos.projecteuler.lib.Euler;

class Euler006 {

    static long solve(long n) {
        long squareOfSum = Euler.sum1ToN(n) * Euler.sum1ToN(n);
        return squareOfSum - Euler.sumOfSquares(n);
    }

}
