package com.morfanos.projecteuler.lib;

import java.util.ArrayList;
import java.util.List;

public class SieveOfEratosthenes {

    public static List<Long> of(long n) {
        if (n <= 1) {
            return new ArrayList<>();
        }

        if (n > Integer.MAX_VALUE) {
            throw new IllegalArgumentException("The size you're asking for is not supported");
        }

        boolean[] arr = new boolean[(int) n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = true;
        }

        for (int i = 2; i < Math.sqrt(n); i++) {
            if (arr[i]) {
                for (int j = i * i; j < n; j += i) {
                    arr[j] = false;
                }
            }
        }

        List<Long> ans = new ArrayList<>();
        for (int i = 2; i < arr.length; i++) {
            if (arr[i]) {
                ans.add((long) i);
            }
        }

        return ans;
    }

}
