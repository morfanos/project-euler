package com.morfanos.projecteuler.p1000.p100.p10;

import com.morfanos.projecteuler.lib.Prime;
import com.morfanos.projecteuler.lib.SieveOfEratosthenes;

import java.util.List;

class Euler007 {

    static long naive(long n) {
        if (n < 1) {
            throw new IllegalArgumentException("must be >=1");
        }

        if (n == 1) {
            return 2;
        }

        long cnt = 1;
        long a = 1;
        do {
            a += 2;
            if (Prime.isPrime(a)) {
                cnt += 1;
            }
        } while (cnt < n);
        System.out.println();
        return a;
    }


    static long solve(long n) {
        List<Long> primes;
        long cnt = 10000;
        do {
            primes = SieveOfEratosthenes.of(cnt);
            cnt *= 2;
        } while (primes.size() < n);
        return primes.get((int) n - 1);
    }

}
