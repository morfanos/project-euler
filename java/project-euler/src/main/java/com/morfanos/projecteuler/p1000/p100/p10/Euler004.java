package com.morfanos.projecteuler.p1000.p100.p10;

import com.morfanos.projecteuler.lib.Euler;

class Euler004 {

    static long solve(long n) {
        // https://en.wikipedia.org/wiki/Palindromic_number
        // Decimal palindromic numbers with an even number factorsOf digits are divisible by 11.
        // Consider the palindromic number, 111111 = 143 * 777
        // It has the form:
        //   P = 100000x + 10000y + 1000z + 100z + 10y + x
        //   P = 100001x + 10010y + 1100z
        //   P = 11(9091x + 910y + 100z)
        //
        // Thus either 143 or 777 is divisible by 11 (143/11=13)
        // We can use this as our "step"
        long min = getMinOfLength(n);
        long max = getMaxOfLength(n);

        if (Euler.length((max * max)) % 2 != 0) {
            return naive(n);
        }

        long largestDivibleBy11 = max;
        for (long i = max; i > min; i--) {
            if (i % 11 == 0) {
                largestDivibleBy11 = i;
                break;
            }
        }

        long curr = Long.MIN_VALUE;
        for (long i = max; i >= min; i--) {
            long step;
            long start;
            if (i % 11 == 0) {
                start = max;
                step = 1;
            } else {
                start = largestDivibleBy11;
                step = 11;
            }
            for (long j = start; j >= i; j -= step) {
                if (i * j < curr) {
                    break;
                }
                if (Euler.isPalindrome(i * j)) {
                    curr = i * j;
                }
            }
        }
        return curr;
    }

    static long naive(long n) {
        long min = getMinOfLength(n);
        long max = getMaxOfLength(n);

        long curr = Long.MIN_VALUE;
        for (long i = max; i >= min; i--) {
            for (long j = i; j >= min; j--) {
                if (i * j < curr) {
                    break;
                }
                if (Euler.isPalindrome(i * j)) {
                    curr = i * j;
                }
            }
        }

        return curr;
    }

    private static long getMinOfLength(long nbDigits) {
        long ans = 1;
        while (nbDigits > 1) {
            ans *= 10;
            nbDigits -= 1;
        }
        return ans;
    }

    private static long getMaxOfLength(long nbDigits) {
        long ans = 9;
        while (nbDigits > 1) {
            ans *= 10;
            ans += 9;
            nbDigits -= 1;
        }
        return ans;
    }

}
