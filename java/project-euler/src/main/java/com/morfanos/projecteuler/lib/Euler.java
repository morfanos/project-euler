package com.morfanos.projecteuler.lib;

public class Euler {

    public static long sum1ToN(long n) {
        // 10
        //
        // 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10
        //
        // using symmetry
        //  1 +  2 +  3 +  4 +  5
        // 10 +  9 +  8 +  7 +  6
        //
        // 11 + 11 + 11 + 11 + 11
        //
        // 5*11 = 55 = 10*(10+1)/2
        //
        // using proof by induction
        //
        // assume sum(1..n) = n*(n+1)/2
        //
        // base case n = 1
        // sum(1..1) = 1*(1+1)/2 = 1
        //
        // assume true when n = k
        // sum(1..k) = k*(k+1)/2
        //
        // inductive step
        // given above
        // sum(1..k+1)
        // = sum(1..k) + (k+1)
        // = k*(k+1)/2 + (k+1)
        // = (k+1)(k+2)/2
        // thus holds for any integer
        return n * (n + 1) / 2;
    }

    public static long sumOfSquares(long n) {
        // assume equation has the form f(x) = ax^3 + bx^2 + cx + d
        // solve via system of linear equations
        //
        // x f(x)
        // 1   1
        // 2   5
        // 3  14
        // 4  30
        //
        // matrix form: WX = Y
        // W
        // 1	1	1	1
        // 8	4	2	1
        // 27	9	3	1
        // 64	16	4	1
        //
        //  Y
        //  1
        //  5
        // 14
        // 30
        //
        // INV(W) * Y = X
        // 0.3333333333
        // 0.5
        // 0.1666666667
        // 0
        //
        // f(x) = 1/6(2x^3 + 3x^2 + 1x + 0)
        //
        // using proof by induction (not shown)
        return (2 * n * n * n + 3 * n * n + n) / 6;
    }

    public static boolean isPalindrome(long n) {
        return n == reverse(n);
    }

    static long reverse(long n) {
        long rev = 0;
        while (n > 0) {
            rev = 10 * rev + n % 10;
            n /= 10;
        }
        return rev;
    }

    public static long length(long n) {
        if (n == 0) {
            return 1;
        }

        if (n < 0) {
            n *= -1;
        }

        long ans = 0;
        while (n > 0) {
            n /= 10;
            ans += 1;
        }
        return ans;
    }

}
