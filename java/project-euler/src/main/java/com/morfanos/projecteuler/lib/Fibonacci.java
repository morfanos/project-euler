package com.morfanos.projecteuler.lib;

public class Fibonacci {
    private long a = 0L;
    private long b = 1L;

    // Fo = 0
    // F1 = 1
    // F2 = F1 + F0
    // ...
    // Fn = Fn-1 + Fn-2
    public long next() {
        long temp = a;
        a = b;
        b += temp;
        return b;
    }
    
}
