package com.morfanos.projecteuler.lib;

public class FibonacciEven {
    private int cnt = 0;
    private long a = 2;
    private long b = 8;

    // Fo = 0
    // F1 = 1
    // F2 = F1 + F0
    // ...
    // Fn = Fn-1 + Fn-2
    //   1 =  1
    //   2 =  2 .............   2
    //   3 =  2 +  1
    //   5 =  3 +  2
    //   8 =  5 +  3 ........   8
    //  13 =  8 +  5
    //  21 = 13 +  8
    //  34 = 21 + 13 ........  34 = 4*8 + 2
    //  55 = 34 + 21
    //  89 = 55 + 34
    // 144 = 89 + 55 ........ 144 = 4*34 + 8
    public long next() {
        if (cnt == 0) {
            cnt += 1;
            return a;
        } else if (cnt == 1) {
            cnt += 1;
            return b;
        } else {
            long temp = a;
            a = b;
            b = 4 * b + temp;
            return b;
        }
    }

}
