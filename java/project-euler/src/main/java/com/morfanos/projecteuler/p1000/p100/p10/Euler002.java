package com.morfanos.projecteuler.p1000.p100.p10;

import com.morfanos.projecteuler.lib.Fibonacci;
import com.morfanos.projecteuler.lib.FibonacciEven;

class Euler002 {

    static long naive(long n) {
        Fibonacci fib = new Fibonacci();
        long a;
        long sum = 0;
        while ((a = fib.next()) < n) {
            if (a % 2 == 0) {
                sum += a;
            }

        }
        return sum;
    }

    static long solve(long n) {
        FibonacciEven fib = new FibonacciEven();
        long a;
        long sum = 0;
        while ((a = fib.next()) < n) {
            sum += a;
        }
        return sum;
    }

}
