package com.morfanos.projecteuler.lib;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

public class Resource {

    public List<String> readAllLines(String filename) {
        ClassLoader classLoader = getClass().getClassLoader();
        URL url = classLoader.getResource(filename);
        Path path = Paths.get(Objects.requireNonNull(url).getPath());
        try {
            return Files.readAllLines(path);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
