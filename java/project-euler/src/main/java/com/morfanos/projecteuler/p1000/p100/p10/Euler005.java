package com.morfanos.projecteuler.p1000.p100.p10;

import com.morfanos.projecteuler.lib.Prime;
import com.morfanos.projecteuler.lib.SieveOfEratosthenes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

class Euler005 {

    static long naiveStreams(long n) {
        List<Long> range = LongStream.rangeClosed(2, n).boxed().collect(Collectors.toList());
        long start = n;
        boolean divides;
        do {
            start += 1;
            long temp = start; // required to satisfy compiler
            divides = range.stream().allMatch(i -> temp % i == 0);
        } while (!divides);
        return start;
    }

    static long naiveIterative(long n) {
        long start = n;
        boolean isDivisible;
        do {
            start += 1;
            isDivisible = true;
            for (long i = 2; i <= n; i++) {
                if (start % i != 0) {
                    isDivisible = false;
                    break;
                }
            }
        } while (!isDivisible);
        return start;
    }

    static long primeFactorisation1(long n) {
        // count number factorsOf prime factorsOf
        //   |2|3|5|7|
        //------------
        //  1| | | | | // ignore, 1 is not prime
        //------------
        //  2|1| | | |
        //------------
        //  3| |1| | |
        //------------
        //  4|2| | | |
        //------------
        //  5| | |1| |
        //------------
        //  6|1| | | |
        //------------
        //  7| | | |1|
        //------------
        //  8|3| | | |
        //------------
        //  9| |2| | |
        //------------
        // 10|1| |1| |
        //------------
        //   |3|2|1|1| <-- max
        // 2^3 * 3^2 * 5^1 * 7^1 = 2520 <-- DONE!
        Map<Long, Long> primeToCount = new HashMap<>();
        for (long i = 2; i <= n; i++) {
            List<Long> primeFactors = Prime.factorsOf(i);
            Map<Long, Long> groupByCount = primeFactors.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            for (Long prime : groupByCount.keySet()) {
                Long temp = primeToCount.getOrDefault(prime, 0L);
                primeToCount.put(prime, Long.max(temp, groupByCount.get(prime)));
            }
        }

        long ans = 1;
        for (Long prime : primeToCount.keySet()) {
            long temp = prime;
            long exp = primeToCount.get(prime);
            while (exp > 1) {
                temp *= prime;
                exp -= 1;
            }
            ans *= temp;
        }

        return ans;
    }

    static long primeFactorisation2(long k) {
        // 2^1 =  2 <= 10
        // 2^2 =  4 <= 10
        // 2^3 =  8 <= 10
        // 2^4 = 16 >  10 <-- gone too far
        // keep 2^3
        //
        // 3^1 =  3
        //     =  9
        //     = 27
        // keep 3^2
        //
        // 3 < sqrt(10) < 4, no need to check higher
        // choose 5^1
        // choose 7^1
        //
        // 2^3 * 3^2 * 5^1 * 7^1 = 2520 <-- DONE!
        //
        // for all primes <= k
        // given pi and k, find a
        // p^a = k
        // a * log(p) = log(k)
        // a = log(k)/log(p)
        long ans = 1;
        double logK = Math.log(k);
        List<Long> primes = SieveOfEratosthenes.of(k);
        for (long prime : primes) {
            long exp = (long) (logK / Math.log(prime));

            long temp = prime;
            while (exp > 1) {
                temp *= prime;
                exp -= 1;
            }
            ans *= temp;
        }
        return ans;
    }

}
