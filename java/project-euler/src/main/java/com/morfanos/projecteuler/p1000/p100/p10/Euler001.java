package com.morfanos.projecteuler.p1000.p100.p10;

import com.morfanos.combination.CombinationFactory;
import com.morfanos.combination.ICombination;
import com.morfanos.projecteuler.lib.Euler;

import java.util.List;
import java.util.stream.LongStream;

class Euler001 {

    static long sumOfMultiplesNaive(List<Long> multiples, long n) {
        return LongStream.range(1, n)
                .filter(a -> multiples.stream().anyMatch(m -> a % m == 0))
                .reduce(0L, Long::sum);
    }

    // using the Inclusion–exclusion principle
    // https://en.wikipedia.org/wiki/Inclusion%E2%80%93exclusion_principle
    //
    // n: 10
    // multiples: 3, 5
    // 3 -> 3, 6, 9
    // 5 -> 5
    // ans: 3 + 5 + 6 +  = 23
    //
    // n: 20
    // multiples: 3, 5
    // + 3   ->  3, 6, 9, 12, 15, 18 -> 3 (1 + 2 + ... + 20/3) = 3 sum(1..6)
    // + 5   ->  5, 10, 15           -> 5 (1 + 2 + 20/5) = 5 sum(1..4)
    // - 3,5 -> 15                   -> 15
    // ans: 3*21 + 5*10 - 15 - 20 = 78
    //
    // n: 100
    // multiples: 2, 3, 5
    // + 2     ->  2 (1 + 2 + ... 100/02) ->  2 sum(1..50)
    // + 3     ->  3 (1 + 2 + ... 100/03) ->  3 sum(1..33)
    // + 5     ->  5 (1 + 2 + ... 100/05) ->  5 sum(1..20)
    // - 2,3   ->  6 (1 + 2 + ... 100/06) ->  6 sum(1..16)
    // - 2,5   -> 10 (1 + 2 + ... 100/10) -> 10 sum(1..10)
    // - 3,5   -> 15 (1 + 2 + ... 100/15) -> 15 sum(1..06)
    // + 2,3,5 -> 30 (1 + 2 + ... 100/30) -> 30 sum(1..03)
    // ans: 2*1275 + 3*561 + 5*210 - 6*136 - 10*55 - 15*21 + 30*6 - 100 = 3682
    static long sumOfMultiples(List<Long> multiples, long n) {
        long sum = -1 * n;
        for (int i = 0; i < multiples.size(); i++) {
            ICombination<Long> combination = CombinationFactory.factory(multiples, i + 1);
            while (combination.hasNext()) {
                List<Long> next = combination.next();
                long a = next.stream().reduce(1L, (x, y) -> x * y);
                long b = a * Euler.sum1ToN(n / a);
                sum += Math.pow(-1, i) * b;
            }
        }
        return sum;
    }

}
