package com.morfanos.projecteuler.p1000.p100.p10;

import com.morfanos.projecteuler.lib.Resource;

import java.util.List;

class Euler008 {

    static long solve(int n, String filename) {
        String input = getInput(filename);
        long ans = Long.MIN_VALUE;
        for (int i = 0; i < input.length() - n + 1; i++) {
            String substring = input.substring(i, i + n);
            long number = Long.parseLong(substring);
            long product = productOfDigits(number);
            ans = Long.max(product, ans);
        }
        return ans;
    }

    private static long productOfDigits(long l) {
        if (l == 0) {
            return 0;
        }
        boolean isNegative = false;
        if (l < 0) {
            l *= -1;
            isNegative = true;
        }
        long ans = 1;
        while (l > 0) {
            ans *= l % 10;
            l /= 10;
        }
        return (isNegative ? -1 : 1) * ans;
    }

    private static String getInput(String filename) {
        Resource resource = new Resource();
        List<String> lines = resource.readAllLines(filename);
        return lines.stream().reduce("", (a, b) -> a + b);
    }

}
