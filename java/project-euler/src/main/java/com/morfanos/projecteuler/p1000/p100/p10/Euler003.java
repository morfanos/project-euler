package com.morfanos.projecteuler.p1000.p100.p10;

class Euler003 {

    static long naive(long n) {
        while (n % 2 == 0) {
            n /= 2;
        }

        long m = 1;
        do {
            m += 2; // all primes > 2 are odd
            while (n % m == 0) {
                n /= m;
            }
        } while (n > 1 || m * m < n);

        return n == 1 ? m : n;
    }
}
