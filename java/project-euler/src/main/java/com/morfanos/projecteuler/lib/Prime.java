package com.morfanos.projecteuler.lib;

import java.util.ArrayList;
import java.util.List;

public class Prime {

    public static boolean isPrime(long n) {
        if (n < 2) {
            return false;
        }

        if (n == 2) {
            return true;
        }

        if (n % 2 == 0) {
            return false;
        }

        for (long a = 3; a * a <= n; a += 2) {
            if (n % a == 0) {
                return false;
            }
        }

        return true;
    }

    public static List<Long> factorsOf(long n) {
        List<Long> primes = new ArrayList<>();

        while (n % 2 == 0) {
            n /= 2;
            primes.add(2L);
        }

        long m = 1;
        do {
            m += 2; // all primes > 2 are odd
            while (n % m == 0) {
                n /= m;
                primes.add(m);
            }
        } while (n > 1 || m * m < n);

        if (n != 1) {
            primes.add(n);
        }

        return primes;
    }

}
