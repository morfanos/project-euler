package com.morfanos.projecteuler.p1000.p100.p10;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class Euler001Test {

    @Test
    public void solve11() {
        assertEquals(23L, Euler001.sumOfMultiplesNaive(Arrays.asList(3L, 5L), 10L));
    }

    @Test
    public void solve12() {
        assertEquals(233168L, Euler001.sumOfMultiplesNaive(Arrays.asList(3L, 5L), 1000L));
    }

    @Test
    public void solve13() {
        assertEquals(78L, Euler001.sumOfMultiplesNaive(Arrays.asList(3L, 5L), 20L));
    }

    @Test
    public void solve14() {
        assertEquals(3682L, Euler001.sumOfMultiplesNaive(Arrays.asList(2L, 3L, 5L), 100L));
    }

    @Test
    public void solve15() {
        assertEquals(396103603960L, Euler001.sumOfMultiplesNaive(Arrays.asList(2L, 3L, 5L, 7L, 11L), 1000000L));
    }

    @Test
    public void solve21() {
        assertEquals(23L, Euler001.sumOfMultiples(Arrays.asList(3L, 5L), 10L));
    }

    @Test
    public void solve22() {
        assertEquals(233168L, Euler001.sumOfMultiples(Arrays.asList(3L, 5L), 1000L));
    }

    @Test
    public void solve23() {
        assertEquals(78L, Euler001.sumOfMultiples(Arrays.asList(3L, 5L), 20L));
    }

    @Test
    public void solve24() {
        assertEquals(3682L, Euler001.sumOfMultiples(Arrays.asList(2L, 3L, 5L), 100L));
    }

    @Test
    public void solve25() {
        assertEquals(396103603960L, Euler001.sumOfMultiples(Arrays.asList(2L, 3L, 5L, 7L, 11L), 1000000L));
    }

}