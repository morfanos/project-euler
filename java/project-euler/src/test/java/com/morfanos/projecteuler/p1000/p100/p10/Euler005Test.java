package com.morfanos.projecteuler.p1000.p100.p10;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class Euler005Test {

    @Test
    public void solve11() {
        assertEquals(2520, Euler005.naiveStreams(10));
    }

    @Ignore("This test is ignored because the implementation is way too slow!")
    @Test
    public void solve12() {
        assertEquals(232792560, Euler005.naiveStreams(20));
    }

    @Test
    public void solve21() {
        assertEquals(2520, Euler005.naiveIterative(10));
    }

    @Test
    public void solve22() {
        assertEquals(232792560, Euler005.naiveIterative(20));
    }

    @Test
    public void solve31() {
        assertEquals(2520, Euler005.primeFactorisation1(10));
    }

    @Test
    public void solve32() {
        assertEquals(232792560, Euler005.primeFactorisation1(20));
    }

    @Test
    public void solve41() {
        assertEquals(2520, Euler005.primeFactorisation2(10));
    }

    @Test
    public void solve42() {
        assertEquals(232792560, Euler005.primeFactorisation2(20));
    }

}
