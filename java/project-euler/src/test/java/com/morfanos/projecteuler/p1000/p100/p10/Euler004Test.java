package com.morfanos.projecteuler.p1000.p100.p10;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler004Test {

    @Test
    public void solve11() {
        assertEquals(9009L, Euler004.naive(2));
    }

    @Test
    public void solve12() {
        assertEquals(906609L, Euler004.naive(3));
    }

    @Test
    public void solve13() {
        assertEquals(9999000000009999L, Euler004.naive(8));
    }

    @Test
    public void solve21() {
        assertEquals(9009L, Euler004.solve(2));
    }

    @Test
    public void solve22() {
        assertEquals(906609L, Euler004.solve(3));
    }

    @Test
    public void solve23() {
        assertEquals(9999000000009999L, Euler004.solve(8));
    }

}