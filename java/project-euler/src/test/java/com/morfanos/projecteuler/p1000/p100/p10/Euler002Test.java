package com.morfanos.projecteuler.p1000.p100.p10;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler002Test {

    @Test
    public void naive11() {
        assertEquals(4613732L, Euler002.naive((long) 4e6));
    }

    @Test
    public void naive12() {
        assertEquals(652484772464328L, Euler002.naive((long) 1e15));
    }

    @Test
    public void solve21() {
        assertEquals(4613732L, Euler002.solve((long) 4e6));
    }

    @Test
    public void solve22() {
        assertEquals(652484772464328L, Euler002.solve((long) 1e15));
    }

}
