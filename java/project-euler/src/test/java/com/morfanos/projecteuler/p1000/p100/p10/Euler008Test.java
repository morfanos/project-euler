package com.morfanos.projecteuler.p1000.p100.p10;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler008Test {

    @Test
    public void solve1() {
        assertEquals(5832L, Euler008.solve(4, "euler008"));
    }

    @Test
    public void solve2() {
        assertEquals(23514624000L, Euler008.solve(13, "euler008"));
    }

}
