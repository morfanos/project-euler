package com.morfanos.projecteuler.lib;

import org.junit.Test;

import static org.junit.Assert.*;

public class EulerTest {

    @Test
    public void sum1ToN() {
        assertEquals(55 * (55 + 1) / 2, Euler.sum1ToN(55));
    }

    @Test
    public void reverse() {
        assertEquals(9, Euler.reverse(9));
        assertEquals(1, Euler.reverse(100));
        assertEquals(21, Euler.reverse(12));
        assertEquals(321, Euler.reverse(123));
        assertEquals(4321, Euler.reverse(1234));
        assertEquals(4321, Euler.reverse(12340));
        assertEquals(40321, Euler.reverse(123040));
        assertEquals(403201, Euler.reverse(1023040));
    }

    @Test
    public void length() {
        assertEquals("length(-123) == 3", 3, Euler.length(-123));
        assertEquals("length(-10)  == 2", 2, Euler.length(-10));
        assertEquals("length(-1)   == 1", 1, Euler.length(-1));
        assertEquals("length(0)    == 1", 1, Euler.length(0));
        assertEquals("length(1)    == 1", 1, Euler.length(1));
        assertEquals("length(10)   == 2", 2, Euler.length(10));
        assertEquals("length(123)  == 3", 3, Euler.length(123));
    }

}
