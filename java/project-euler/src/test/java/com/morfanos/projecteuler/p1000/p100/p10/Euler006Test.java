package com.morfanos.projecteuler.p1000.p100.p10;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler006Test {

    @Test
    public void solve11() {
        assertEquals(2640, Euler006.solve(10));
    }

    @Test
    public void solve12() {
        assertEquals(25164150, Euler006.solve(100));
    }

}
