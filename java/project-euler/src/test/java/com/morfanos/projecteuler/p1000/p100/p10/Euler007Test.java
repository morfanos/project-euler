package com.morfanos.projecteuler.p1000.p100.p10;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler007Test {

    @Test
    public void solve11() {
        assertEquals(13, Euler007.naive(6));
    }

    @Test
    public void solve12() {
        assertEquals(104743, Euler007.naive(10001));
    }

    @Test
    public void solve21() {
        assertEquals(13, Euler007.solve(6));
    }

    @Test
    public void solve22() {
        assertEquals(104743, Euler007.solve(10001));
    }

}
