package com.morfanos.projecteuler.p1000.p100.p10;

import org.junit.Test;

import static org.junit.Assert.*;

public class Euler003Test {

    @Test
    public void solve1() {
        assertEquals(29L, Euler003.naive(13195L));
    }

    @Test
    public void solve2() {
        assertEquals(6857L, Euler003.naive(600851475143L));
    }

}